//themesManager.js
'use strict';

var exec = require('child_process').exec;

var isVerbose = undefined,
    finalCallback = undefined,
    cwd = null,
    themesDirPath = cwd + '/wp-content/themes/';

var getGITData = function getGITData(themeId, promcb) {
	console.log(cwd);
	//let cwd = cwd;
	var path = themesDirPath;
	console.log(cwd, themesDirPath);

	var prepGitCmd = 'git clone ' + themeId;
	var gitCmd = exec(prepGitCmd, { cwd: cwd + '/wp-content/themes/' }, function (err, stdout, stderr) {
		if (err) return console.log(err);
		if (isVerbose) {
			if (stdout) console.log('stdout: ' + stdout);
			if (stderr) console.log('stderr: ' + stderr);
		}

		// resolve Promise
		promcb('git');
	});
};

var getHTTPData = function getHTTPData(themeId, promcb) {

	promcb('http');
};

var getWPThemeData = function getWPThemeData(themeId, promcb) {

	promcb('wptheme');
};

var installTheme = function installTheme(themeId) {

	var fnToCall = null;
	if (themeId.match(/^http.*\.git$/) /* !== -1*/) {
			// install wp theme by git clone
			console.log('case: ' + 'themeId.match(/^http.*\.git$/)');
			fnToCall = getGITData;
		} else if (themeId.match(/^http.*/) /* !== -1*/) {
			// install wp theme by url
			console.log('case: ' + 'themeId.match(/^http.*/)');
			fnToCall = getHTTPData;
		} else {
		// install wp theme by name/slug
		console.log('case: ' + 'else');
		fnToCall = getWPThemeData;
	}

	var p = new Promise(function (resolve, reject) {
		if (fnToCall) fnToCall(themeId, resolve);
	});
	p.then(function (val) {
		var res = undefined;

		switch (val) {
			case 'git':
				console.log('switchCase: GIT');
				//return 'theme downloaded'
				res = 'git theme downloaded';
				break;
			case 'http':
				console.log('switchCase: http');
				res = 'http theme downloaded';
				break;
			case 'wptheme':
				console.log('switchCase: wptheme');
				res = 'wptheme theme downloaded';

				break;
			default:
				return false;
				break;
		}
		return res;
	}).then(function (val) {
		console.log(val);
		finalCallback();
	}).catch(function (reason) {
		console.log(reason);
	});
};

var themesManager = function themesManager(cwdarg, themes, isVerbose, callback) {

	console.log('cwdarg', cwdarg);
	console.log('themes', themes);
	console.log('isVerbose', isVerbose);
	isVerbose = isVerbose;

	finalCallback = callback;

	cwd = cwdarg;
	console.log('cwd', cwd);

	console.log('themesManager.js running');
	console.log('callback', callback);
	console.log('finalCallback', finalCallback);

	for (var i = themes.length - 1; i >= 0; i--) {
		console.log(themes[i]);
		installTheme(themes[i]);
	};

	//if(finalCallback) finalCallback();
};

module.exports = themesManager;
//# sourceMappingURL=themesManager.js.map
