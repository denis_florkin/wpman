//wp-config.js
'use strict';

module.exports = function (installPath, dbname, keysSalt, wpConf) {
  'use strict';

  var fs = require('fs'),
      exec = require('child_process').exec; /*,
                                            process = require('process');*/

  var template = ['<?php\n/**\n * The base configuration for WordPress\n *\n * The wp-config.php creation script uses this file during the\n * installation. You don\'t have to use the web site, you can\n * copy this file to "wp-config.php" and fill in the values.\n *\n * This file contains the following configurations:\n *\n * * MySQL settings\n * * Secret keys\n * * Database table prefix\n * * ABSPATH\n *\n * @link https://codex.wordpress.org/Editing_wp-config.php\n *\n * @package WordPress\n */\n\n// ** MySQL settings - You can get this info from your web host ** //\n\n/** The name of the database for WordPress */\ndefine(\'DB_NAME\', \'',
  //database_name_here
  '\');\n\n/** MySQL database username */\ndefine(\'DB_USER\', \'',
  /*root*/
  '\');\n\n/** MySQL database password */\n\ndefine(\'DB_PASSWORD\', \'',
  /*root*/
  '\');\n\n/** MySQL hostname */\ndefine(\'DB_HOST\', \'',
  /*localhost*/
  '\');\n\n/** Database Charset to use in creating database tables. */\ndefine(\'DB_CHARSET\', \'utf8\');\n\n/** The Database Collate type. Don\'t change this if in doubt. */\ndefine(\'DB_COLLATE\', \'\');\n\n/**#@+\n * Authentication Unique Keys and Salts.\n *\n * Change these to different unique phrases!\n * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}\n * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.\n *\n * @since 2.6.0\n */',
  /*
  define('AUTH_KEY',         'put your unique phrase here');
  define('SECURE_AUTH_KEY',  'put your unique phrase here');
  define('LOGGED_IN_KEY',    'put your unique phrase here');
  define('NONCE_KEY',        'put your unique phrase here');
  define('AUTH_SALT',        'put your unique phrase here');
  define('SECURE_AUTH_SALT', 'put your unique phrase here');
  define('LOGGED_IN_SALT',   'put your unique phrase here');
  define('NONCE_SALT',       'put your unique phrase here');
  */
  '/**#@-*/\n/**\n * WordPress Database Table prefix.\n *\n * You can have multiple installations in one database if you give each\n * a unique prefix. Only numbers, letters, and underscores please!\n */\n\n $table_prefix  =\'',
  // 'wp_'

  '\';/**\n * For developers: WordPress debugging mode.\n *\n * Change this to true to enable the display of notices during development.\n * It is strongly recommended that plugin and theme developers use WP_DEBUG\n * in their development environments.\n *\n * For information on other constants that can be used for debugging,\n * visit the Codex.\n *\n * @link https://codex.wordpress.org/Debugging_in_WordPress\n */\ndefine(\'WP_DEBUG\', false);\n\n/* That\'s all, stop editing! Happy blogging. */\n\n/** Absolute path to the WordPress directory. */\nif ( !defined(\'ABSPATH\') )\n\tdefine(\'ABSPATH\', dirname(__FILE__) . \'/\');\n\n/** Sets up WordPress vars and included files. */\nrequire_once(ABSPATH . \'wp-settings.php\');'];

  var parseData = function parseData() {
    var res = template[0];
    // append db name
    res += dbname;
    res += template[1];

    // user
    res += wpConf.dbUser;
    res += template[2];

    // pwd
    res += wpConf.dbPwd;
    res += template[3];
    // host
    res += wpConf.dbHost;
    res += template[4];

    // data from the curl
    res += keysSalt;
    res += template[5];
    //table prefix
    res += 'wp_' + dbname + '_';
    res += template[6];

    return res.toString();
  };

  fs.closeSync(fs.openSync(installPath + '/wp-config.php', 'w'));

  var cmd = parseData();

  fs.writeFileSync(installPath + '/wp-config.php', cmd);

  console.log('the wp-config.php file was succefully generated and written to disk');

  return parseData();
};
//# sourceMappingURL=wp-config.js.map
