//
// #!/usr/bin/env node --harmony

'use strict'

// require dependencies
const
	program    = require('commander'),
	prompt     = require('prompt'),
	exec       = require('child_process').exec,
	chalk      = require('chalk'),
	mysql      = require('mysql'),
	fs         = require('fs'),

	pkg        = require('../package.json'),
	wpConf     = require('../wp.config.json'),
	wpc        = require('../lib/wp-config.js'),
	connection = require('../lib/connection.js'),
	plugman    = require('../lib/pluginsManager.js'),
	themesman  = require('../lib/themesManager.js');


	const findInvoqPathUi = () => {
		let cmdFindInvoqPath = exec('pwd',(err,stdout,stderr)=>{
			if(err){ return console.log(err) }

		    if(stderr) console.log(`stderr: ${stderr}`);
			if(stdout) {
				//invoqPath = stdout.trim();
				//return invoqPath;
				console.log('####uiinvoquedPAth : '+stdout.trim())



				var util = require('util');
				var logTSID = Date.now();

				var log_file = fs.createWriteStream(stdout.trim() + `/_debug.log`, {flags : 'w',defaultEncoding: 'utf8'});
				//var log_file = fs.createWriteStream(__dirname + `/debug_${logTSID}_.log`, {flags : 'w'});
				var log_stdout = process.stdout;

				console.log = function(d) { //
				  log_file.write(util.format(d) + '\n');
				  log_stdout.write(util.format(d) + '\n');
				};




			}
		})
	}
	//findInvoqPathUi()
	//var localLog = findInvoqPath ()
	//console.log(invoqPath)
/**
  *		# LOGGIN STUFF INTO A FILE
  */

	/*var util = require('util');
	var logTSID = Date.now();

	var log_file = fs.createWriteStream(__dirname + `/_debug.log`, {flags : 'r+',defaultEncoding: 'utf8'});
	//var log_file = fs.createWriteStream(__dirname + `/debug_${logTSID}_.log`, {flags : 'w'});
	var log_stdout = process.stdout;

	console.log = function(d) { //
	  log_file.write(util.format(d) + '\n');
	  log_stdout.write(util.format(d) + '\n');
	};*/






// global variables
let
	isVerbose,
	custom,
	invoqPath,
	wpName,
	installName        = null,
	wpInstallPath      = null,
	wpInstallDirectory = null,
	fullWpInstallPath  = null,
	//wpConf             = null,

	promptedServerPath = null;



/* // debug :
	(function() {
	    var childProcess = require("child_process");
	    var oldSpawn = childProcess.spawn;
	    function mySpawn() {
	        console.log('spawn called');
	        console.log(arguments);
	        var result = oldSpawn.apply(this, arguments);
	        return result;
	    }
	    childProcess.spawn = mySpawn;
	})();
*/



/*const getWPConfigJSON = () => {
	if(isVerbose) console.log('gathering the wp.config.json data')
	let tmpWpConf = require('../wp.config.json');
	wpConf = tmpWpConf;
	return wpConf;
}
*/

const downloadWpCore = (callback)=> {
	//let prefixDirectory = wpInstallDirectory != null ?
	//	wpInstallDirectory : `${invoqPath}`;

	//fullWpInstallPath = `${prefixDirectory}`

	let cwd = invoqPath ? invoqPath : custom.path;
	console.log(cwd)
	//if(wpInstallDirectory){
		let cmd = exec(`mkdir ${wpName}`,{cwd:cwd},(err,stdout,stderr) => {
			if(err)	return console.log(err)
			if(stderr) console.log(`stderr: ${stderr}`);
		    if(isVerbose){
		    	if(stdout) console.log(`stdout: ${stdout}`);
				console.log(chalk.green('SUCCESS :'),'wp directory succesfully created')
				console.log('starting the wp core download...')
		    }
		    	let cwd = invoqPath ? invoqPath : custom.path;
		    	cwd = cwd+wpName;
		    	custom.path = cwd;
			let dlwp = exec('wp core download',{cwd:cwd},(err,stdout,stderr)=>{
				if(err)	return console.log(err)
				if(stderr) console.log(`stderr: ${stderr}`);
			    if(isVerbose){
			    	if(stdout) console.log(`stdout: ${stdout}`);
					console.log('wp core succeffuly donwloaded')
				}
				callback('success')
			})
		})

	//}else{	// !!!!! DRY GODAMNIT
		// let dlwp = exec('wp core download'/*,{cwd:fullWpInstallPath}*/,(err,stdout,stderr)=>{
		// 	if(err){
		// 		throw err
		// 	}
		//     if(isVerbose){
		//     	if(stdout) console.log(`stdout: ${stdout}`);
		// 		console.log(chalk.green('SUCCESS :'),'wp core succeffuly donwloaded')
		// 	}
		// 	if(stderr) console.log(`stderr: ${stderr}`);

		// 	callback(null)
		// })
	//}
}


const makeSqlCall = ()=> {

	// todo => allow wpname to not be given and use a timestamp or smth
	//let wpname = name || Date.now()

	//prep statement
	let sql = "CREATE DATABASE ??"
	let inserts = [wpName]
	sql = mysql.format(sql,inserts)

	connection.connect()
	connection.query(sql,(err,rows,fields)=>{
		if(err)	return console.log(err)
	    //if(stderr) console.log(`stderr: ${stderr}`);
		//if(stdout) return console.log(`stdout: ${stdout}`);
		if(isVerbose){
			// need better test !!!
			console.log('database '+wpName+' successfully created')
		}

	})
	connection.end();

	//return wpname
}

const openInBrowser = ()=>{
	console.log('openning in that browser')
	// let openIt = exec(`open ${custom.url}/wp-admin`,(err,stdout,stderr)=>{
	let openIt = exec(`open ${custom.url}`,(err,stdout,stderr)=>{
			if(err) return console.log(err);

			if(stdout) console.log(`stdout: ${stdout}`);
		    if(stderr) console.log(`stderr: ${stderr}`);

		    if (isVerbose) console.log('wp successfuly openned in browser!')

		})
}


const makeWpConfigPHPFile = ()=> {
	console.log('starting php config file ')
	// grab wp salt secret key
	let p = new Promise((resolve,reject)=>{
		let grabUid = exec('curl https://api.wordpress.org/secret-key/1.1/salt/ ',(err,stdout,stderr)=>{
			if(err){
				throw err
			}
		    if(stdout){
		    	let prefixDirectory = wpInstallDirectory != null ? wpInstallDirectory : invoqPath;

		    	let wpcfgFile = wpc(custom.path,wpName,stdout)

		    	resolve(wpcfgFile)
		    }
		})
	})
	p.then((val)=>{
		// prompt.message = chalk.blue('• § PROMPT ')
		// prompt.delimiter = chalk.blue(':')
		// prompt.start();
		// prompt.get({
		// 	properties:{
		// 		'websiteUrl':{
		// 			description:'Enter the web adress of the wp ("http://path/to/wp/")'
		// 		}
		// 	}
		// },(err,result)=>{
			//if(err) return console.log(err)

			// promptedServerPath = result.websiteUrl;
			// promptedServerPath = 'http://localhost/projects/';
			//promptedServerPath = wpConf.defaultServerPath + wpname;
			let serverPath = custom.url ? custom.url : wpConf.defaultServerPath;
			//console.log(wpConf.defaultServerPath)
			console.log(wpName)
			console.log(serverPath)

			console.log('Success! starting the wp installation now...')
			console.log('wpInstallPath',wpInstallPath)

			//let cwd = wpInstallDirectory ? wpInstallDirectory : invoqPath
			//if(wpInstallDirectory) { cwd = wpInstallDirectory}

			let cwd = invoqPath ? invoqPath : custom.path;


			let prepCmd = `wp core install --path=${cwd} --url=${serverPath} --admin_user=${wpConf.adminName} --title=${wpName} --admin_email=${wpConf.adminMail} --admin_password=${wpConf.adminPwd}`
			let cmd = exec(prepCmd,(err,stdout,stderr)=>{
				if(err) return console.log(err);

				if(stdout) console.log(`stdout: ${stdout}`);
			    if(stderr) console.log(`stderr: ${stderr}`);

			    console.log('wp successfuly installed! Check out ',serverPath)
			    console.log(wpConf.plugins.length)
			    console.log('wpConf.plugins.length <= 0',wpConf.plugins.length <= 0)

			    if(wpConf.plugins.length <= 0){
			    	// no plugin to install
			    	openInBrowser();
			    }else{
			    	console.log('wp.config.json has plugins')
			    	// there is plugins to install and activate
			    	let wpConfThemesArr = wpConf.themes;

			    	if(wpConfThemesArr.length >= 0){
			    		//plugman then only thememan
						plugman(cwd,wpConf.plugins,isVerbose,(/*cwd,wpConfThemesArr,isVerbose,openInBrowser*/)=>{
									let cb = ()=>{
										themesman(cwd,wpConfThemesArr,isVerbose,openInBrowser);
									}
									return cb
								}(cwd,wpConfThemesArr,isVerbose,openInBrowser)
							);
			    	}else{
			    		// just plugman
			    		plugman(cwd,wpConf.plugins,isVerbose,openInBrowser);
			    	}


			    }

			})

		//})

	})
	.catch((reason)=>{
		console.log(reason)
	})

}


const myProgrammFn = () => {

	// set the verbose flag
	/*if(options.verbose) isVerbose = true;
	if(wpname){
		installName = wpname
		wpInstallPath = `wp-${installName}/`;
		if(typeof(options) != 'object'){
			wpInstallDirectory = options
		}else{

		}
	}


	wpInstallPath = `./wp-${installName}/`;*/

	// let cmdFindInvoqPath = exec('pwd',(err,stdout,stderr)=>{
	// 	if(err){ return console.log(err) }

	//     if(stderr) console.log(`stderr: ${stderr}`);
	// 	if(stdout) {
	// 		invoqPath = stdout.trim();
	// 	}

	// 	if(isVerbose)
	// 		console.log('verbose mode installation')

		// get config
		// let wpconf = getWPConfigJSON()

		// dl wp
		let p = new Promise((resolve,reject)=>{
			downloadWpCore(resolve);
		})

		p.then((val)=>{	// val : null
			// create db
			//let t = makeSqlCall();
			return makeSqlCall();
			//return t;
		 })
		 .then((val)=>{
		 	// make wp-config.php file and launch install
		 	let c = makeWpConfigPHPFile()
		 	return c;
		 })
		 .then((val)=>{
		 	// make wp core install/DL
		 	if(isVerbose) console.log('starting wp core download now...')
		 })
		 .catch((reason)=>{
			if(reason) console.log("Caught ERROR : ",reason)
		 })

	// })
}

const findInvoqPath = ()=>{
		let cmdFindInvoqPath = exec('pwd',(err,stdout,stderr)=>{
			if(err){ return console.log(err) }

		    if(stderr) console.log(`stderr: ${stderr}`);
			if(stdout) {
				invoqPath = stdout.trim();
				//return invoqPath;
	//console.log(invoqPath)
			}
		})
	}

const programmSetUp = (wpname,options) =>{
	console.log('wpname '+wpname)
	console.log('options.verbose '+options.verbose)
	console.log('options.path '+options.path)
	console.log('options.url '+options.url)
	//console.log(invoqPath)
	findInvoqPathUi();
	// set the verbose flag
	if(options.verbose) isVerbose = true

	if(wpname) wpName = wpname

	if(options.here){
		findInvoqPath()
	}else{
		if(options.url || options.path){
			if(options.url && options.path){
				custom = {};
				custom.url = options.url + wpname
				custom.path = options.path
			}else{
				return console.log('If you provide a custom url for the installation, you also have to provide a path, and vice versa')
			}
		}else{
			// use default conf
		}
	}

	myProgrammFn();

	// console.log('does return cut me ?')	// yep it does, Cool :p


}


// declare programm sub command options
program
	.version('0.0.1')
	.command('install <wpname>','take care of setting up the wordpress for you.')//.option('-s, --silent', 'silent install')
	.command('i <wpname>','short alias fo the install cmd.')//.option('-s, --silent', 'silent install')
	.option('-v, --verbose', 'verbose mode installation.')
	.option('-h, --here', 'install here flag.')
	.option('-p, --path [path]', 'specify an absolute path to a directory for the wp installation.')
	.option('-u, --url [url]', 'specify the server url to access the wp.')
	.action(programmSetUp)

// parse
program
	.parse(process.argv)

module.exports = programmSetUp

