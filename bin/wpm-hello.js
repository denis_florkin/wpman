// wpm-hello.js

const
	program = require('commander');


const myProgrammFn = (name,options) => {
	console.log('Hello ' + name)
}

module.exports = myProgrammFn;


program
	.version('0.0.1')
	.command('install','take care of setting up the wordpress for you. \n You need to provide valid config file (wp.config.json) \n and required access to the program')//.option('-s, --silent', 'silent install')
	.option('-v, --verbose', 'verbose isntall')
	.action(myProgrammFn)

program
	.parse(process.argv)

