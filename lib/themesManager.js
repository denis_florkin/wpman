//themesManager.js
'use strict';

const
	exec = require('child_process').exec;

let
	isVerbose,
	finalCallback,
	cwd = null,
	themesDirPath = `${cwd}/wp-content/themes/`;

const getGITData = (themeId,promcb)=>{
	console.log(cwd)
	//let cwd = cwd;
	let path = themesDirPath;
	console.log(cwd,themesDirPath)

	let prepGitCmd = `git clone ${themeId}`;
	let gitCmd = exec(prepGitCmd,{cwd:`${cwd}/wp-content/themes/`},(err,stdout,stderr)=>{
		if(err) return console.log(err)
		if(isVerbose){
			if(stdout) console.log(`stdout: ${stdout}`);
		    if(stderr) console.log(`stderr: ${stderr}`);
		}

			// resolve Promise
			promcb('git');
	})
}

const getHTTPData = (themeId,promcb)=>{

	promcb('http');
}

const getWPThemeData = (themeId,promcb)=>{

	promcb('wptheme');
}

const installTheme = (themeId)=>{

	let fnToCall = null;
	if(themeId.match(/^http.*\.git$/)/* !== -1*/){
		// install wp theme by git clone
		console.log('case: '+ 'themeId.match(/^http.*\.git$/)')
		fnToCall = getGITData

	}else if(themeId.match(/^http.*/)/* !== -1*/){
		// install wp theme by url
		console.log('case: '+ 'themeId.match(/^http.*/)')
		fnToCall = getHTTPData
	}else{
		// install wp theme by name/slug
		console.log('case: '+ 'else')
		fnToCall = getWPThemeData
	}



	let p = new Promise((resolve,reject)=>{
		if(fnToCall) fnToCall(themeId,resolve);
	})
	p.then((val)=>{
		let res;

			switch(val){
				case 'git':
					console.log('switchCase: GIT')
					//return 'theme downloaded'
					res = 'git theme downloaded'
					break;
				case 'http':
					console.log('switchCase: http')
					res = 'http theme downloaded'
					break;
				case 'wptheme':
					console.log('switchCase: wptheme')
					res = 'wptheme theme downloaded'

					break;
				default:
					return false;
					break;
			}
			return res;
		})
		.then((val)=>{
			console.log(val);
			finalCallback();
		})
		.catch((reason)=>{
			console.log(reason)
		})

}

const themesManager = (cwdarg,themes,isVerbose,callback)=>{

	console.log('cwdarg',cwdarg)
	console.log('themes',themes)
	console.log('isVerbose',isVerbose)
	isVerbose = isVerbose;

	finalCallback = callback;

	cwd = cwdarg;
	console.log('cwd',cwd)

	console.log('themesManager.js running');
	console.log('callback',callback);
	console.log('finalCallback',finalCallback);



	for (let i = themes.length - 1; i >= 0; i--) {
		console.log(themes[i])
		installTheme(themes[i])
	};




	//if(finalCallback) finalCallback();
}

module.exports = themesManager;