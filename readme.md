
# wpm

#### `wpm` ,  A node tool to manage wordpress

This is a little nodejs layer built on top of the [wp-cli](http://wp-cli.org/)
to allow easy use of config file for setting up wordpress in no time



the goal is a 'one command setup of your personnal tailor made wordpress installation'

### `wp` ( [wp-cli](http://wp-cli.org/) ) is going to be installed globaly, you can use it directly after the installation

### `wpm` assume git is installed on the machine



## On time set up

#### this is a > 5 min, 5 steps, set up for all time fun

### 1. Download the [zip](https://bitbucket.org/denis_florkin/node-cli-app_wpmaker/downloads) or clone the repo

		$ git clone https://bitbucket.org/denis_florkin/node-cli-app_wpmaker

### 2. install packages

##### WARNING
Note ! : the postinstall script of the npm installation
is going to install [wp-cli](http://wp-cli.org/) as `wp` on your system
and it's also goig to make it available gloabaly,
so if you already have a global command under the alias `wp` or `wpm`
do not install this package as it will overwritte you existing command

		$ npm install




### 3. Create a "link" for the command to be in the global path (need sudo)

##### WARNING
Note ! : the npm link command is going to make the `wpm` cli app
available everywhere in the system (global path).
That means that if you already have a command name `wpm`
Do not use npm link as it will overwritte you existing wpm command

		$ sudo npm link



### 4. create/populate the wp.config.json file

in the repertory there's a wp.config.json (if not create one)
fill the data with your infos, it should look like this :

		{
			"adminName":"billy",
			"adminMail":"denisflorkin@gmail.com",
			"adminPwd":"billy",
			"plugins":[
					"wordpress-seo",
					"wp-retina-2x",
					"ninja-forms",
					"wp-smushit",
					"advanced-custom-fields"
				],
			"themes":[
					"https://bitbucket.org/denis_florkin/noomia_bare_wp_boilerplate.git"
				]
		}

### 5. set up sql credentials

configure the 'lib/connection.js' file for your sql environement (name, host, pwd)

it should look like this :

		const connection = mysql.createConnection({
			host:'localhost',
			user: 'root',
			password:'root'
		})


#### 6. if you are using MAMP, there is one final step

(yeah I lied, there is a sixth step)

##### you will need to make your mamp `php` the main globally available  php command in your path (this is for wp-cli to work properly)

* more on why here : [http://wp-cli.org/](http://wp-cli.org/)

* #### how to set it up here : [stackoverflow : how-to-override-the-path-of-php-to-use-the-mamp-path](http://stackoverflow.com/questions/4145667/how-to-override-the-path-of-php-to-use-the-mamp-path/29990624#29990624)


# That's it!
If everything went according to the plan,
you should now be able to use the command
from anywhere in your system

##### WATCH for the prompt when using `install` or `i` (in blue in the terminal) !!!

this is going to be fixed with config default value as soon as possible...


### check the help out by simply typing :

	   	$ wpm

##### same goes for wp (ctrl-c to stop)

		$ wp

There is 2 basic scenario :


* #### install here mode

You create a directory and invoq the command  from within the directory

		$ wpm install <wpname>

You do not need to provide a directory argument, only name is required


for instance invoqing the command from the directory

/Users/bily/document/myWordpressDir/

without `--directory </abs/path/>` option
would result in the wordpress files being downloaded/installed
directly at the root of 'myWordpressDir'



_ create 'myWordpressDir' yourself


			.../myWordpressDir/
				|-(empty...)


_ invoque the command from the direcotry


		$ wpm install theNameOf-MyWP


_ after

			.../myWordpressDir/
					|- wp-content
						|- ...
						|- ...
					|- wp-includes
						|- ...
						|- ...
					|- wp-admin
						|- ...
						|- ...
					|- wp-config.php
					|- index.php
					|-  ...
					|-  ...


* #### install there mode

Specify where you want the installation to take place.

Do that using the `--directory </absolute/path/to/myWordpressDir/>` option

#### this would, in the folder 'to', create the folder 'myWordpressDir'  for you


_ before calling the command (the directory 'to' exists)


			.../to/
				|-(empty...)



_ invoque the command from where ever you want/are


		$ wpm install theNameOf-MyWP --directory /absolute/path/to/myWordpressDir/


_ after


			.../to/myWordpressDir/
					|- wp-content
						|- ...
						|- ...
					|- wp-includes
						|- ...
						|- ...
					|- wp-admin
						|- ...
						|- ...
					|- wp-config.php
					|- index.php
					|-  ...
					|-  ...


For instance here the directory `myWordpressDir` will be created by the wpm (it will throw an erro if it exists)

and the files will be downloaded/installed there



----




# USAGE


##### WATCH for the prompt when using `install` or `i` (in blue in the terminal) !!!


### say hello :

		$ wpm hello bily


### install (set up wp and db):

	   $ wpm install theNameOf-my_wordpress


### install verbose mode :

		$ wpm install theNameOf-my_wordpress -v

or with long option argument synthax and short command alias (i for install)

		$ wpm i --vebose theNameOf-my_wordpress


### install in a specific directory :

		$ wpm i theNameOf-my_wordpress --directory /Abs/Path/to/directory




# Road map

* start install by cleanning "everything" (posts,page,plugins,theme) (IN PROGRESS)
* install plugin(s) from info in wp.config.json (IN PROGRESS)
* install theme(s) from infos in wp.config.json (IN PROGRESS)
* add an 'always the same directory' installation/configuration option/flag to avoid the prompt
* git repo init
* git repo server config autopush then gitignore and set up local env
* allow to use all wp-cli commands trouhgt (and rename wpm-install to 'wpm-setup' or 'wpm-init')




