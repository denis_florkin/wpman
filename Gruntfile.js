module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    clean:{
      dist:['dist/']
    },
    babel: {
      options: {
        sourceMap: true,
        presets: ['babel-preset-es2015'],
        ignore:'node_modules/'
      },
      build: {
        files: [
          {
            expand: true,       // Enable dynamic expansion.
            cwd: 'dev/',        // Src matches are relative to this path.
            src: ['**/*.js','**/wpm'], // Actual pattern(s) to match.
            //dest: 'tmp/',   // Destination path prefix.
            dest: 'dist/',   // Destination path prefix.
            //ext: '.es5.js'   // Dest filepaths will have this extension.
            ext: '.js'   // Dest filepaths will have this extension.
          }
        ]
      }
    }
  })

  // Load the plugin that provides the "uglify" task.
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-babel');


  grunt.registerTask('default',['clean:dist','babel'])



};