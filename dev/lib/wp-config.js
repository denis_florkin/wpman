//wp-config.js
'use strict';
module.exports = (installPath,dbname,keysSalt,wpConf) => {
'use strict';

	const
		fs = require('fs'),
		exec = require('child_process').exec/*,
		process = require('process');*/


	const template = [
		`<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //

/** The name of the database for WordPress */
define('DB_NAME', '`,
	//database_name_here
`');

/** MySQL database username */
define('DB_USER', '`,
	/*root*/
	`');

/** MySQL database password */

define('DB_PASSWORD', '`,
	/*root*/
	`');

/** MySQL hostname */
define('DB_HOST', '`,
	/*localhost*/
	`');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */`,
 /*
define('AUTH_KEY',         'put your unique phrase here');
define('SECURE_AUTH_KEY',  'put your unique phrase here');
define('LOGGED_IN_KEY',    'put your unique phrase here');
define('NONCE_KEY',        'put your unique phrase here');
define('AUTH_SALT',        'put your unique phrase here');
define('SECURE_AUTH_SALT', 'put your unique phrase here');
define('LOGGED_IN_SALT',   'put your unique phrase here');
define('NONCE_SALT',       'put your unique phrase here');
*/
`/**#@-*/
/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */

 $table_prefix  ='`,
 // 'wp_'


`';/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');`
	]

	const parseData = () => {
		var res = template[0]
		// append db name
		res += dbname
		res += template[1]

		// user
		res += wpConf.dbUser
		res += template[2]

		// pwd
		res += wpConf.dbPwd
		res += template[3]
		// host
		res += wpConf.dbHost
		res += template[4]

		// data from the curl
		res += keysSalt;
		res += template[5]
		//table prefix
		res += `wp_${dbname}_`
		res += template[6]

		return res.toString();
	}



	fs.closeSync(fs.openSync(installPath+'/wp-config.php', 'w'));

	let cmd = parseData();

	fs.writeFileSync(installPath+'/wp-config.php',cmd)

	console.log('the wp-config.php file was succefully generated and written to disk');

	return parseData();


}