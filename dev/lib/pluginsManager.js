//pluginsManager.js
'use strict';

/*

const
	exec = require('child_process').exec,
	fixPath   = require('fix-path');

	console.log(process.env.PATH)
	fixPath();
	console.log(process.env.PATH)


let callbackFinal;
let pluginsAlreadyInstalled = 0;
let pluginsLength = 0;
let installPath;
let isVerbose;


const removeDefaultPlugins = (installPath,callback)=>{
	// let prepRemoveAllPlugCmd = `php ${process.cwd()+'/wp-cli.phar'} plugin delete $( php ${process.cwd()+'/wp-cli.phar'} plugin list --field=name) --path=${installPath}`
	// 	let removeAllPlugCmd = exec(prepRemoveAllPlugCmd,{cwd:installPath},(err,stdout,stderr)=>{
	// 		if(err) return console.log(err);
	// 		if(stdout) console.log(`stdout: ${stdout}`);
	// 	   	if(stderr) console.log(`stderr: ${stderr}`);

	 		if(callback) callback();

	// 	})
}



const installAllPlugins = (plugins,installPath,isVerbose)=>{
		let prepInstallAllPlugCmd = `php ${process.cwd()+'/wp-cli.phar'} plugin install ${plugins.join(' ')} --path=${installPath} --activate`
		let removeAllPlugCmd = exec(prepInstallAllPlugCmd,(err,stdout,stderr)=>{
			if(err) return console.log(err);
			if(stdout) console.log(`stdout: ${stdout}`);
		   	if(stderr) console.log(`stderr: ${stderr}`);

			if(callbackFinal) callbackFinal();
		})
}



const pluginsManager = (installPath,plugins,isVerbose,callback) => {

	console.log(installPath);
	//installPath = installPath;
	pluginsLength = plugins.length
	pluginsAlreadyInstalled = 0;
	callbackFinal = callback;
	//isVerbose = isVerbose;
	console.log('plugman staring its work')
	console.log('Starting the plugins installation and activation');

	removeDefaultPlugins(installPath,( ()=>{

		let retfn = ()=>{
			installAllPlugins(plugins,installPath,isVerbose)
		}
		return retfn




	} )(plugins,installPath,isVerbose) )

}*/

module.exports = (()=>{

	const
		exec = require('child_process').exec,
		fixPath   = require('fix-path');

		console.log(process.env.PATH)
		fixPath();
		console.log(process.env.PATH)


	let callbackFinal;
	let pluginsAlreadyInstalled = 0;
	let pluginsLength = 0;
	let installPath;
	let isVerbose;


	const removeDefaultPlugins = (installPath,callback)=>{
		let prepRemoveAllPlugCmd = `php ${process.cwd()+'/wp-cli.phar'} plugin delete $( php ${process.cwd()+'/wp-cli.phar'} plugin list --field=name) --path=${installPath}`
			let removeAllPlugCmd = exec(prepRemoveAllPlugCmd,{cwd:installPath},(err,stdout,stderr)=>{
				if(err) return console.log(err);
				if(stdout) console.log(`stdout: ${stdout}`);
			   	if(stderr) console.log(`stderr: ${stderr}`);

		 		if(callback) callback();

			})
	}



	const installAllPlugins = (plugins,installPath,isVerbose)=>{
			let prepInstallAllPlugCmd = `php ${process.cwd()+'/wp-cli.phar'} plugin install ${plugins.join(' ')} --path=${installPath} --activate`
			let removeAllPlugCmd = exec(prepInstallAllPlugCmd,(err,stdout,stderr)=>{
				if(err) return console.log(err);
				if(stdout) console.log(`stdout: ${stdout}`);
			   	if(stderr) console.log(`stderr: ${stderr}`);

				if(callbackFinal) callbackFinal();
			})
	}





	const pluginsManager = (installPath,plugins,isVerbose,callback) => {
		console.log(installPath);
		//installPath = installPath;
		pluginsLength = plugins.length
		pluginsAlreadyInstalled = 0;
		callbackFinal = callback;
		//isVerbose = isVerbose;
		console.log('plugman staring its work')
		console.log('Starting the plugins installation and activation');

		removeDefaultPlugins(installPath,( ()=>{

			let retfn = ()=>{
				installAllPlugins(plugins,installPath,isVerbose)
			}
			return retfn




		} )(plugins,installPath,isVerbose) )

	}
	return	pluginsManager

})();
//module.exports = pluginsManager;

